import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { QuantityPicker } from 'react-qty-picker';

export default function CourseView() {

	const navigate = useNavigate();

	// To be able to obtain user ID so we can checkout a user
	const { user } = useContext(UserContext);

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [sizeAndColor, setSizeAndColor] = useState("");
	const [src, setSrc] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);


	const checkout = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/${productId}/checkout`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if(data) {

				console.log(quantity)

				Swal.fire({
					title: "Order successful!",
					imageUrl: `${src}`,
					text: `Your order of ${quantity}x ${name} with total amount of Php ${quantity * price} is being processed please check your email for confirmation`
				})

				navigate("/products");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {

		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setSizeAndColor(data.sizeAndColor);
			setSrc(data.src);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	return(
		<Container>
			<Row className=" m-1 d-flex">
				<Col className="p-3 m-1">
					<Card>
						<Card.Img className="productViewImgStyling" variant="top" src={src}/>
					</Card>
				</Col >

				<Col className="p-3 m-1">
					<Card>
					    <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Text className="productViewSizeAndColorStyling">{sizeAndColor}</Card.Text>
					        <Card.Text className="pt-5">{description}</Card.Text>	
					        <QuantityPicker smooth min={1} value={quantity} onChange={(value) => {setQuantity(value)}}/>		        
					        <Card.Text className="pt-3">Price:</Card.Text>
					        <Card.Text>&#x20B1; {price}</Card.Text>
					        <Card.Text className="totalAmount">Total Amount: &#x20B1; {price * quantity}</Card.Text>

					        {user.id !== null
					        	?
					        	<Button variant="warning" onClick={() => checkout(productId)} style={{ width: '100%'}}>Buy</Button>
					        	:
					        	<Button variant="danger" as={Link} to="/login" style={{ width: '100%'}}>Log in to Purchase</Button>
					        }
					    </Card.Body>
					</Card>
				</Col>

					
				
			</Row>
		</Container>
	)
}