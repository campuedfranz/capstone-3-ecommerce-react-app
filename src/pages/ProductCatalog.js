import { Fragment, useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';


export default function ProductCatalog() {
	

	const [products, setProducts] = useState([]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
				)
			}))
		})

	}, [])

	return (
		<Fragment>
			<h3 className=" mt-2 pt-3 pb-2 productCatalogHeading">Products</h3>
			<p className="productCatalogText"> Stay on top of the game while looking fresh and trendy with footwears from brands like Adidas, Nike, etc.</p>
			<div className="d-flex flex-wrap justify-content-center">
				{products}
			</div>
		</Fragment>
	)
}