import Image from 'react-bootstrap/Image';
import heroImageA from  '../assets/images/hero-image1.png'

function FluidExample() {
  return <Image src={heroImageA}  fluid />;
}

export default FluidExample;