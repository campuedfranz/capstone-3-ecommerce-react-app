import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

	console.log(productProp);

	const {_id, name, sizeAndColor, src, price } = productProp;

    return (
        <Card className="p-1 m-2 d-flex d-inline-flex text-decoration-none cardStyling" as={Link} to={`/products/${_id}`}>
            <Card.Img variant="top" src={src} style={{ height: '350px' }}/>
            <Card.Body>
                <Card.Title as="h6">{name}</Card.Title>
                <Card.Text className="sizeAndColorStyling">{sizeAndColor}</Card.Text>
                <Card.Text className="priceStyling">&#x20B1; {price}</Card.Text>
                {/*<Button variant="primary" as={Link} to={`/products/${_id}`} style={{ width: '100%'}}>Details</Button>*/}
            </Card.Body>
        </Card>
    )
}